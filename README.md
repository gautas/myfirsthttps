# myfirsthttps

FastAPI + docker + https + traefik following https://www.youtube.com/watch?v=7N5O62FjGDc and https://dev.to/tiangolo/deploying-fastapi-and-other-apps-with-https-powered-by-traefik-5dik

## Notes
### Step 1: poetry
```sh
curl -sSL https://install.python-poetry.org | python3 -
poetry init
```

### Step 2: fastapi

```sh
poetry add fastapi "uvicorn[standard]"
```

### Step 3: uvicorn
```sh
poetry run uvicorn app.main:app
```

### docker network
```sh
docker network create traefik-public
```